<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="viewport" content="width=device-width" />
    <link href="https://fonts.googleapis.com/css?family=Raleway:200,500,600|Roboto+Condensed:400,700|Josefin+Sans&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo content_url() ?>/themes/projektownia/style.php" />
    <link rel="stylesheet" type="text/css" href="<?php echo content_url() ?>/custom/font-awesome/css/font-awesome.min.css">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo content_url() ?>/themes/projektownia/scripts.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <?php wp_head(); ?>
    <?php if(is_single()): ?>
        <meta property="og:image" content="<?php $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id(),'large', true); echo $thumb_url[0]; ?>"/>
    <?php endif; ?>
</head>
<body <?php body_class(); ?>>
    <div id="fb-root"></div>
    <div id="wrapper" class="hfeed">
    <header id="header" role="banner">
        <section id="branding">
            <div>
                <div id="logo-container"><a href="<?php echo site_url() ?>">
                   <img id="header-logo" src="<?php echo content_url()?>/images/logo.svg" width="180"/>
                </a></div>
                <a href="<?php echo site_url() ?>">
                    <?php echo file_get_contents("wp-content/custom/headerText.txt")?> 
                </a>
            </div>
        </section>
        <nav id="menu" role="navigation">
            <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
        </nav>
    </header>
    <div id="container">
        