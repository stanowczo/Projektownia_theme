<?php 
    if(strpos(get_bloginfo('name'),'Blog')!==false):
        $args = array( 'numberposts' => '1', 'post_status' => 'publish' );
        $recent_posts = wp_get_recent_posts($args);
        $recent = $recent_posts[0];   
        $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id($recent["ID"]),'large', true); 
        file_put_contents("wp-content/custom/blogNewestPostLink.txt", get_permalink($recent["ID"]));
        file_put_contents("wp-content/custom/blogNewestPostImageLink.txt", $thumb_url[0]); 
        file_put_contents("wp-content/custom/blogNewestPostTitle.txt", get_the_title($recent["ID"])); 
    endif;
    
    function infoProjektowanie_func( $atts ){
        $a = shortcode_atts( array(
            'area' => '?',
            'date' => '?',
        ), $atts );
        $const1 = '<div class="info-projektowanie"><div></div><p>powierzchnia: ';
        $const2 = '</p><div></div><p>data realizacji: ';
        $const3 = '</p><div></div></div>';
        $result = $const1 . $a['area'] . $const2 . $a['date'] . $const3;
        return $result;
    }
    add_shortcode( 'infoProjektowanie', 'infoProjektowanie_func' );
?>

<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'entry' ); ?>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
<footer class="footer">
<?php get_template_part( 'nav', 'below-single' ); ?>
</footer>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>