<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header <?php if(strpos(get_bloginfo('name'),'Blog')===false): echo("class='header-toTheLeft'"); endif;?>>
    <?php if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
    <?php if ( is_singular() ) { echo '<h1 class="entry-title">'; } else { echo '<h2 class="entry-title">'; } ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a><?php if ( is_singular() ) { echo '</h1>'; } else { echo '</h2>'; } ?> <?php edit_post_link(); ?>
</header>
<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
<?php if(strpos(get_bloginfo('name'),'Blog')!==false): ?>
    <div class="postHorizontalLine"></div>
    <div class="postFooterSocial">
        <ul>
            <li class="social">Udostępnij:</li>
            <li class="social pinterest">
                <a href="https://pinterest.com/pin/create/link/?url=<?php echo get_permalink()?>" target="_blank" rel="nofollow"><span class="fa fa-lg fa-pinterest"></span></a>
            </li>
            <li class="social facebook">
                <a href="https://www.facebook.com/sharer.php?u=<?php echo get_permalink()?>" target="_blank" rel="nofollow"><span class="fa fa-lg fa-facebook"></span></a>
            </li>
            <li  class="social facebook-like">
                <div class="fb-like" data-href="<?php echo get_permalink()?>" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
            </li>
        </ul>
    </div>
    <div class="postHorizontalLine"></div>
    <?php if(is_single()): ?>
        <div class="post-tags"><?php the_tags('','','') ?></div>
        <div class="postHorizontalLine"></div>
    <?php endif; ?>
<?php endif; ?>
</article>

