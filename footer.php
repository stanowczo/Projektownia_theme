<div class="clear"></div>
</div>
<footer id="footer" role="contentinfo">
<div id="copyright">
    <div class="postHorizontalLine <?php if(strpos(get_bloginfo('name'),'Blog')===false): echo('postHorizontalLineWide'); endif;?>" id="footerHorizontalLine"></div>
    <?php echo sprintf( __( '%1$s %2$s %3$s. Wszelkie prawa zastrzeżone.', 'projektownia' ), '&copy;', date( 'Y' ), esc_html( get_bloginfo( 'name' ) ) ); echo sprintf( __( ' Motyw Wordpressa: %1$s.', 'projektownia' ), '<a href="http://design.stanowczo.pl/">Stanowczo Design</a>' ); ?>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>