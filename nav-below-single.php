<?php if(strpos(get_bloginfo('name'),'Blog')!==false): ?>
    <nav id="nav-below" class="navigation" role="navigation">
    <div class="nav-previous"><?php previous_post_link( '%link', '<span class="meta-nav">&larr;</span> %title' ); ?></div>
    <div class="nav-next"><?php next_post_link( '%link', '%title <span class="meta-nav">&rarr;</span>' ); ?></div>
    </nav>
    <div class="below-instagram">
        <div class="postHorizontalLine"></div>
        <div class="post-tags">INSTAGRAM</div>
        <div class="postHorizontalLine"></div>
        <?php echo do_shortcode("[jr_instagram id='2']");?>
    </div>
<?php endif; ?>