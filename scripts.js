//facebook "like" script
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

//pinterest on wp-tiles gallery
function prepareUrl(urlToPrep){
    return urlToPrep.replace(/:/g,"%3A").replace(/\//g,'%2F');
}

//show comments section on a single page
function showComments(){
    $(".comments-without-header").show("slow");
    $(".show-comments").hide();
}

function centerContactLabel(){
    $("#main-page-label-contact").css({left:($("#main-page-label-contact").parent().width() - $("#main-page-label-contact").width())/2 - 10 + 'px'});
}

$(document).ready(function(){    
    //check whether comment section should be visible from the beginning
    var currentPage = window.location.href;
    if(currentPage.includes('#comment')){
        showComments();    
    };
    
    //show comments button
    $(".show-comments").click(function(){
        showComments();     
    });
    
    //pinterest on wp-tiles gallery
    $(".wp-tiles-tile-bg").click(function(){
        var img = $(this).css('background-image');
        img = img.replace('url(','').replace(')','').replace(/\"/gi, "");
        img = prepareUrl(img);
        var site = window.location.href;
        site = prepareUrl(site);
        window.open("http://pinterest.com/pin/create/bookmarklet/?is_video=false&url=" + site + "&media=" + img);
    });
    
    $(".main-page-tiles .grayed").mouseenter(function(){
        $(this).removeClass('tile-outOfFocus').addClass('tile-focused'); 
    });
    
    $(".main-page-tiles .grayed").mouseleave(function(){
        $(this).removeClass('tile-focused').addClass('tile-outOfFocus'); 
    });
    
    window.setTimeout(centerContactLabel,500);
});

$(window).resize(function () {    
    window.setTimeout(centerContactLabel,500);
});


