<section class="entry-meta">
<?php if(strpos(get_bloginfo('name'),'Blog')!==false): ?>
    <span class="entry-date"><?php the_time( get_option( 'date_format' ) ); ?></span>
    <div class="headerHorizontalLine"></div>
<?php endif; ?>
</section>
