<?php
//Recent post info
if(strpos(get_bloginfo('name'),'Blog')===false):
    function blogRecentPostLink_func( $atts ){
        return file_get_contents("blog/wp-content/custom/blogNewestPostLink.txt");
    }
    add_shortcode( 'blogRecentPostLink', 'blogRecentPostLink_func' );

    function blogRecentPostTitle_func( $atts ){
        return file_get_contents("blog/wp-content/custom/blogNewestPostTitle.txt");
    }
    add_shortcode( 'blogRecentPostTitle', 'blogRecentPostTitle_func' );
endif;

//Fill page with post tiles
if(strpos(get_bloginfo('name'),'Blog')===false):
    function projektowaniePostsTiles_func( $atts ){
        $a = shortcode_atts( array(
            'cat' => '',
            'num' => 9,
        ), $atts );
        
        $const1 = '<div class="col col-projektowanie col-md-4 col-sm-6 col-xs-12 grayed tile-outOfFocus"><a href="';
        $const2 = '"><div class="tile tile-projektowanie" style="background-image:url(';
        $const3 = ');"><div class="tile-projektowanie-hover"></div><p>';
        $const4 = '</p></div></a></div>';
        $tiles = '';
        
        $postsFilter = array( 'category_name' => $a['cat'], 'post_status' => 'publish', 'numberposts' => $a['num'] );
        $posts = get_posts($postsFilter);
        
        foreach ($posts as $post){
            $postLink = get_permalink($post);
            $thumb_url = wp_get_attachment_image_src(get_post_thumbnail_id($post),'large', true);
            $postImage = $thumb_url[0];
            if(has_excerpt($post)===true){
                $postTitle = get_the_excerpt($post);    
            } else {
                $postTitle = get_the_title($post);
            }

            $tiles .= $const1 . $postLink . $const2 . $postImage . $const3 . $postTitle . $const4;  
        }
        return $tiles;
    }
    add_shortcode( 'projektowaniePostsTiles', 'projektowaniePostsTiles_func' );
endif;
?>

<?php get_header(); ?>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header">
<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
</header>
<section class="entry-content entry-content-wide">
<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
<?php the_content(); ?>
<div class="entry-links"><?php wp_link_pages(); ?></div>
</section>
</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>