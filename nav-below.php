<?php global $wp_query; if ( $wp_query->max_num_pages > 1 ) { ?>
<nav id="nav-below" class="navigation nav-pagination" role="navigation">
    <ul>
        <?php 
            if (strpos(get_bloginfo('name'),'Blog')!==false) {
                $page_format = '?paged=%#%';
            } else {
                $page_format = '/page/%#%';
            };
            $big = 999999999;
            $args = array(
                'base'               => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format'             => $page_format,
                'total'              => $wp_query->max_num_pages,
                'current'            => max( 1, get_query_var('paged') ),
                'prev_text'          => __('<span class="fa fa-lg fa-angle-left"></span>'),
                'next_text'          => __('<span class="fa fa-lg fa-angle-right"></span>'),
            );
            echo paginate_links( $args ); 
        ?>
    </ul>
</nav>
<?php } ?>
<?php if(strpos(get_bloginfo('name'),'Blog')!==false): ?>
    <div class="below-instagram">
        <div class="postHorizontalLine"></div>
        <div class="post-tags">INSTAGRAM</div>
        <div class="postHorizontalLine"></div>
        <?php echo do_shortcode("[jr_instagram id='2']");?>
    </div>
<?php endif; ?>